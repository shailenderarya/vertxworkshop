package workshop.apihandler;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.vertx.core.AsyncResult;
import io.vertx.core.MultiMap;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import io.vertx.core.Handler;
import workshop.utils.*;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class isSiteReachable implements Handler<RoutingContext> {

  private static final VertxLogger logger = VertxLogger.VertxLoggingFactory.getLogger(isSiteReachable.class);

  @Override
  public void handle(RoutingContext routingContext) {

      logger.error(null, "request Received");
      MultiMap mmap = routingContext.request().params();
      String site = mmap.get("site");
      checkServiceAndResp(routingContext, site);

  }

  public void checkServiceAndResp(RoutingContext routingContext,String site){
    EventBus eventBus = routingContext.get(Constants.ROUTING_BUS);

    DeliveryOptions options = getDefaultDeliveryOptions(routingContext);

    ClientHttpRequest clientReq = new ClientHttpRequest();
    clientReq.setMethod("/");
    clientReq.setService(site);
    eventBus.send(Constants.EVENT_HTTP_GET, JsonUtils.object2Json(clientReq).toString(), options,
        resp -> {
          try {
            this.handleClientVerticleResp(routingContext, resp);
          } catch (Exception e) {
            logger.debug(routingContext, "Exception occurred ", e);
            ExceptionHelper.handleExceptionAndRespond(routingContext, e);
          }
        });

  }


  private void handleClientVerticleResp(RoutingContext context,
                                        AsyncResult<Message<Object>> resp) throws IOException {

    logger.debug(context,
        " HttpClient  Response received = " + resp.succeeded() + " " + resp);
    Optional<EventBusResponse> eventBusResponseOptional = EventBusUtils.getEventBusResp(resp);
    if (!eventBusResponseOptional
        .isPresent()) {
      throw new RuntimeException("Invalid Response from HttpClientVerticle");
    }
    EventBusResponse eventBusResp = eventBusResponseOptional.get();
    logger.debug(context,"event Bus Response = " + eventBusResp + " routingContext ==" + context);
    Response servResp ;
    HttpServerResponse httpResp = context.request().response();
    httpResp.putHeader("Content-type", "application/json");

    if (!eventBusResp.isSuccess()) {
      Response.ResponseError respError;
      if (eventBusResp.isUserError()){
        respError = new Response.ResponseError("INVALID_SITE", "The Site specified is either invalid or not reachable");
        httpResp.setStatusCode(400);
      }
      else{
        respError = new Response.ResponseError("SYSTEM_ERROR", "System Error got from Client verticle");

        httpResp.setStatusCode(500);
      }
      List<Response.ResponseError> errors = new LinkedList<>();
      errors.add(respError);
      servResp = ResponseFactory.createFailureResp(context,errors);

    }
    else{
      ObjectNode data = JsonUtils.newJsonObject();
      data.put("success",true);
      data.put("message","site is reachable");
      servResp = ResponseFactory.createSuccessResp(context,data);
      httpResp.setStatusCode(200);
    }

    httpResp.end(JsonUtils.object2Json(servResp).toString());


  }


  public static DeliveryOptions getDefaultDeliveryOptions(RoutingContext context) {
    DeliveryOptions options = new DeliveryOptions();
    options.setSendTimeout(context.get(Constants.VERTICLE_TIMEOUT))
        .addHeader(Constants.LOG_ID, context.get(Constants.LOG_ID))
        .addHeader(Constants.START_TIME, new Long(System.currentTimeMillis()).toString());
    return options;
  }




}
