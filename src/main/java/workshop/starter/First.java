package workshop.starter;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;

public class First {


  public static void main(String[] args){
    VertxOptions vops = new VertxOptions();
    System.out.println("event pool size "+ vops.getEventLoopPoolSize());
    Vertx vertx = Vertx.vertx();

    DeploymentOptions options = new DeploymentOptions();
    options.setInstances(Runtime.getRuntime().availableProcessors());

    vertx.deployVerticle("workshop.verticles.HttpVerticle",options);
    vertx.deployVerticle("workshop.verticles.HttpClientVerticle",options);

  }


}
