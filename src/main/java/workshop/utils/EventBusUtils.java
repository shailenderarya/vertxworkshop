package workshop.utils;

import com.fasterxml.jackson.databind.JsonNode;
import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.Message;

import java.util.Optional;

public class EventBusUtils {




  public static void sendSuccessResponse(Message<Object> message, JsonNode data) {
    EventBusResponse resp = new EventBusResponse();
    resp.setSuccess(true);
    resp.setSystemError(false);
    resp.setUserError(false);
    resp.setData(data);
    message.reply(JsonUtils.object2Json(resp).toString());

  }

  public static void sendErrorResponse(Message<Object> message, boolean systemError,
                                       String errorCode) {
    EventBusResponse resp = new EventBusResponse();
    resp.setSuccess(false);
    resp.setSystemError(systemError);
    resp.setUserError(!systemError);
    resp.setErrorCode(errorCode);
    message.reply(JsonUtils.object2Json(resp).toString());

  }

  public static  Optional<EventBusResponse> getEventBusResp(
      AsyncResult<Message<Object>> response) {
    if (!response.succeeded()) {
      return Optional.empty();
    }

    Object output = response.result().body();
    return Optional.of(JsonUtils.jsonStr2Object(EventBusResponse.class,
        output.toString()));
  }
}

