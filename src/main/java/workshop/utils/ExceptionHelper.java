package workshop.utils;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.ext.web.RoutingContext;
import workshop.utils.Response.ResponseError;

import java.util.LinkedList;
import java.util.List;

public class ExceptionHelper {


    private static final VertxLogger logger = VertxLogger.VertxLoggingFactory
        .getLogger(ExceptionHelper.class);

    public static void handleExceptionAndRespond(RoutingContext rc, Exception e) {
      HttpServerResponse resp = rc.request().response();
      resp.putHeader("Content-type", "application/json");

      List<ResponseError> errs = null;

        resp.setStatusCode(500);
        errs = new LinkedList<ResponseError>();
        errs.add(new ResponseError("INTERNAL_SERVER_ERROR", "Error Occurred We are looking into it"));

      Response output = ResponseFactory.createFailureResp(rc, errs);
      logger.warn(rc, "Failure response data = " + JsonUtils.object2Json(output).get("errors")
          +  " for request " + rc.request().absoluteURI() );
      resp.end(JsonUtils.object2Json(output).toString());
    }





  }
