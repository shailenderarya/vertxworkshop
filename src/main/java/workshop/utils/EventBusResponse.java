package workshop.utils;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;


@Data
public class EventBusResponse {
    private boolean success = true;
    private boolean userError;
    private boolean systemError;
    private String errorCode ;
    private JsonNode data;
  }


