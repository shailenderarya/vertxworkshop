package workshop.utils;

public class Constants {

  public static final String LOG_ID = "loggingId";
  public static final String START_TIME = "stTime";
  public static final String API = "api";
  public static final String VERTICLE_TIMEOUT = "defaultVerticleTimeOut";
  public static final String HTTP_CLIENT_VERTICLE_TIMEOUT = "httpClientVerticleTimeout";
  public static final String ROUTING_BUS = "routingBus";
  public static final String EVENT_HTTP_GET = "http.Get";

}
