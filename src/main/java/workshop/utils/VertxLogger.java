package workshop.utils;

import io.vertx.core.eventbus.Message;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import io.vertx.ext.web.RoutingContext;


/**
 * This class is to log the requestId corresponding to each log. Req Id will be
 * generated per request and will be presend in both mesasge and routing context
 * */
public final class VertxLogger {

  private Logger logger;

  private VertxLogger(Logger logger2) {
    this.logger = logger2;
  }

  public static final class VertxLoggingFactory {

    public static VertxLogger getLogger(Class clazz) {
      return new VertxLogger(LoggerFactory.getLogger(clazz));
    }
  }

  public void debug(Object context, String message) {
    String id = getRequestId(context);
    logger.debug(id + " " + message);

  }

  public void warn(Object context, String message) {
    String id = getRequestId(context);
    logger.warn(id + " " + message);
  }

  public void error(Object context, String message) {
    String id = getRequestId(context);
    logger.error(id + " " + message);
  }

  public void info(Object context, String message) {
    String id = getRequestId(context);
    logger.info(id + " " + message);
  }


  public void debug(Object context, String message, Throwable e) {
    String id = getRequestId(context);
    logger.debug(id + " " + message + e, e);

  }

  public void warn(Object context, String message, Throwable e) {
    String id = getRequestId(context);
    logger.warn(id + " " + message + e, e);
  }

  public void error(Object context, String message, Throwable e) {
    String id = getRequestId(context);
    logger.error(id + " " + message + e , e);
  }

  public void info(Object context, String message, Throwable e) {
    String id = getRequestId(context);
    logger.info(id + " " + message + e, e);
  }

  private String getRequestId(Object context) {
    String id = null;
    if (context instanceof Message) {
      id = ((Message) context).headers().get("loggingId");
    } else if (context instanceof RoutingContext) {
      id = ((RoutingContext) context).get("loggingId");
    }

    if (StringUtils.isEmpty(id)) {
      id = "_DEFAULT_";
    }
    return id;
  }
}
