package workshop.utils;

import com.fasterxml.jackson.databind.JsonNode;
import io.vertx.ext.web.RoutingContext;
import lombok.Data;

import java.util.List;

public final class ResponseFactory {

  public static Response createSuccessResp(RoutingContext context, JsonNode data) {
    Response.Meta meta = new Response.Meta();
    meta.setRequestId((String) context.get("loggingId"));
    SuccessResponse resp = new SuccessResponse();
    resp.setData(data);
    resp.setMeta(meta);
    return resp;
  }

  public static Response createFailureResp(RoutingContext context,
                                           List<Response.ResponseError> errors) {
    Response.Meta meta = new Response.Meta();
    meta.setRequestId((String) context.get("loggingId"));
    FailureResponse resp = new FailureResponse();
    meta.setSuccess(false);
    resp.setErrors(errors);
    resp.setMeta(meta);
    return resp;
  }

  @Data
  static class FailureResponse extends Response {
    private List<Response.ResponseError> errors;

  }
  @Data
  static class SuccessResponse
      extends Response {
    private JsonNode data;
  }
}