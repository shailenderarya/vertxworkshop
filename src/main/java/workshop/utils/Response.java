package workshop.utils;

import lombok.Data;

@Data
public class Response {
  private Meta meta;

  @Data
  public static class Meta{
    private String requestId;
    private boolean success = true;
  }

  @Data
  public static class ResponseError{
    private String errorCode;
    private String errorMessage;

    public ResponseError(String errCode, String errMessage){
      this.errorCode = errCode;
      this.errorMessage = errMessage;
    }
  }

}
