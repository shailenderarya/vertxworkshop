package workshop.utils;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.HashMap;
import java.util.Map;


    import lombok.EqualsAndHashCode;
    import lombok.Getter;
    import lombok.NoArgsConstructor;
    import lombok.Setter;
    import lombok.ToString;

    import com.fasterxml.jackson.databind.JsonNode;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
 public class ClientHttpRequest {

  @Setter
  private String service;
  @Setter
  private String method;
  @Setter
  private JsonNode data;

  @Setter
  private String queryParams;

  private Map<String, String> headers = new HashMap<>();

  public Map<String, String> withHeader(String key, String value) {
    headers.put(key, value);
    return headers;
  }

  public Map<String, String> removeHeader(String key) {
    headers.remove(key);
    return headers;
  }
}
