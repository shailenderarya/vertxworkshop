package workshop.utils;


import java.util.Map;

import lombok.Data;

import com.fasterxml.jackson.databind.JsonNode;

@Data
public class ClientHttpResponse {
  private JsonNode data;
  private Map<String, String> responseHeaders;
  private int httpStatusCode;

}
