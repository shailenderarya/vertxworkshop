package workshop.utils;


import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.vertx.core.logging.Logger;

public class JsonUtils {


  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final VertxLogger logger = VertxLogger.VertxLoggingFactory.getLogger(JsonUtils.class);

  private JsonUtils() {
  }

  public static JsonNode object2Json(Object object) {
    return OBJECT_MAPPER.valueToTree(object);
  }

  public static ObjectNode newJsonObject() {

    return OBJECT_MAPPER.createObjectNode();
  }

  public static <T> T bytes2Object(Class<T> clazz, byte[] buffer) {
    T t = null;
    try {
      t = OBJECT_MAPPER.readValue(buffer, clazz);

    } catch (Exception e) {
      logger.debug(null,"Exception in covnerting the Request", e);

    }
    return t;
  }

  public static <T> T jsonStr2Object(Class<T> clazz, String jsonString) {
    Object t = null;
    try {
      t = OBJECT_MAPPER.readValue(jsonString, clazz);
    } catch (Exception e) {
      logger.debug(null,"Exception in covnerting the Request", (Throwable) e);
    }
    return (T) t;
  }

  public static <T> T mmap2Object(MultiMap mmap, Class<T> clazz) {
    if (mmap.isEmpty()) { return null; }
    HashMap<String, String> map = new HashMap<String, String>(mmap.size());
    for (Map.Entry entry : mmap.entries()) {
      map.put((String) entry.getKey(), (String) entry.getValue());
    }
    return JsonUtils.map2Object(map, clazz);
  }

  public static <T> T map2Object(Map<String, String> map, Class<T> clazz) {
    Object t = null;
    t = OBJECT_MAPPER.convertValue(map, clazz);
    return (T) t;
  }

  public static <T> T mapO2Object(Map<String, Object> map, Class<T> clazz) {
    Object t = null;
    t = OBJECT_MAPPER.convertValue(map, clazz);
    return (T) t;
  }

  public static Map<String, Object> object2Map(Object ob) {
    return (Map) OBJECT_MAPPER.convertValue(ob, (Class) Map.class);
  }

  public static <T> T json2Object(Class<T> clazz, JsonNode requestJson) throws IOException {
    Object t = null;
    t = OBJECT_MAPPER.readerFor(clazz).readValue(requestJson);
    return (T) t;
  }

  public static JsonNode jsonObject2Node(JsonObject jsonObject) {
    ObjectNode node = (ObjectNode) OBJECT_MAPPER.convertValue((Object) jsonObject,
        (Class) ObjectNode.class);
    return node;
  }

  public static byte[] getBytes(Object object) {
    try {
      return OBJECT_MAPPER.writeValueAsBytes(object);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      return null;
    }
  }

  static {
    OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false);
    OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
    OBJECT_MAPPER.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
  }
}