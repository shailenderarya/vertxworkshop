package workshop.verticles;


import java.util.HashMap;
import java.util.Map;

import io.vertx.core.http.*;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.node.ObjectNode;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import workshop.utils.*;

public class HttpClientVerticle extends AbstractVerticle {

  private static final VertxLogger logger = VertxLogger.VertxLoggingFactory
      .getLogger(HttpClientVerticle.class);

  @Override
  public void start(Future<Void> startFuture) throws Exception {
    HttpClientOptions options = new HttpClientOptions();
    logger.debug(null,"starting Client verticle");
    options.setConnectTimeout( 5000);
    options.setSsl(true);
    options.setTrustAll(true);
    options.setDefaultPort(443);

    HttpClient client = vertx.createHttpClient(options);

    EventBus eventBus = vertx.eventBus();
    registerMessageHandlers(client, eventBus);

  }

  private String getBaseUrl(ClientHttpRequest clientReq) {
    if (StringUtils.isBlank(clientReq.getQueryParams())) { return clientReq.getMethod(); }
    return clientReq.getMethod() + "?" + clientReq.getQueryParams();
  }



  private void makeRequest(HttpClient client, int port, String host, String baseUrl, ClientHttpRequest req, Message<Object> message){
    io.vertx.core.http.HttpClientRequest vertxClientReq = null;

    logger.debug(message, "base url - " + baseUrl + " Client HttpRequest = " + req);
    vertxClientReq = client.request(HttpMethod.GET, port, host, baseUrl, response -> {
      response.bodyHandler(buffer -> {


        if(response.statusCode() >= 400){
            errorHandler(new Exception("ClientService API Internal Server Error."), host, message);
        }else {
          responseHandler(req, buffer, response, host, message);
        }
      });
    });

    vertxClientReq.exceptionHandler(exception -> {
      logger.debug(message, "ClientService API failed with exception = " + exception );
      errorHandler(exception, host, message);
    });

    logger.debug(message, "vertxClientRequest = " + vertxClientReq);
    vertxClientReq.setTimeout(6000);

    vertxClientReq.end();

  }


  private void registerMessageHandlers(HttpClient client, EventBus eventBus) {
    MessageConsumer<Object> consumer = eventBus.consumer(Constants.EVENT_HTTP_GET);
    consumer.handler(message -> {

      ClientHttpRequest req = JsonUtils.jsonStr2Object(ClientHttpRequest.class, (String) message.body());
      logger.debug(message, " GET request received = " + req);
      int port = 443;
      String host = req.getService();
      if ( StringUtils.isBlank(host)){
        host = "google.com";
      }

      logger.debug(message, "host = " + host + " port = " + port + " method = " +getBaseUrl(req));

      makeRequest(client, port, host, getBaseUrl(req), req, message);


    });
  }

  private void responseHandler(ClientHttpRequest req, Buffer buffer,
                               io.vertx.core.http.HttpClientResponse response, String method, Message<Object> message){

    logger.debug(message, "ClientService API response received for Req = [" + req + " ] + response = [ status = "
        + response.statusCode() + " responseBody = " + buffer + " ]");

    ClientHttpResponse resp = new ClientHttpResponse();
    //commenting the conversion of Data and just checking status code and
//    resp.setData(JsonUtils.bytes2Object(ObjectNode.class, buffer.getBytes()));
    resp.setHttpStatusCode(response.statusCode());
    resp.setResponseHeaders(multiMap2Map(response.headers()));

    long timeTaken = System.currentTimeMillis() - Long.parseLong(message.headers().get(Constants.START_TIME));
    EventBusUtils.sendSuccessResponse(message, JsonUtils.object2Json(resp));

  }

  private void errorHandler(Throwable exception , String method, Message<Object> message){

    logger.debug(message, "exception occurred", exception);
    long timeTaken = Long.parseLong(message.headers().get(Constants.START_TIME))
        - System.currentTimeMillis();
    EventBusUtils.sendErrorResponse(message, false, null);

  }

  private Map<String, String> multiMap2Map(MultiMap mmap) {
    Map<String, String> hmap = new HashMap<>();
    for (String key : mmap.names()) {
      hmap.put(key, mmap.get(key));
    }
    return hmap;
  }
}