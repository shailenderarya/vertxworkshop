package workshop.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import workshop.utils.*;
import workshop.utils.VertxLogger.VertxLoggingFactory;
import workshop.utils.Response.ResponseError;
import workshop.apihandler.isSiteReachable;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class HttpVerticle extends AbstractVerticle {

  private static final VertxLogger logger = VertxLoggingFactory.getLogger(HttpVerticle.class);

  @Override
  public void start(Future<Void> startFuture) {

      logger.debug(null,"staring the Http Verticle");
      startHttpServer();

  }




  private void startHttpServer() {
      HttpServer httpServer = vertx.createHttpServer();

      Router router = Router.router(vertx);
      router.route().handler(BodyHandler.create());
      router.route().handler(routingContext -> this.initRoutingContext(routingContext));
       addConfigRoutes(router, config());
      router.route().handler(routingContext -> {
        sentWrongRequest(routingContext);
      });
      Integer port = config().getInteger("http.port",8999);
      logger.debug("null", "port received = " + port);
      httpServer.requestHandler(router::accept).listen(port);
      logger.debug(null, "successfully Started Http server on port :[" + port + "]");
    }

  private void addConfigRoutes(Router router, JsonObject config){

    Route route = router.route(HttpMethod.GET,"/reachable/:site");
    route.handler(new isSiteReachable());


  }


  private void initRoutingContext (RoutingContext routingContext){
    UUID uuid = UUID.randomUUID();
    routingContext.put(Constants.LOG_ID,uuid.toString());
    routingContext.put(Constants.START_TIME,System.currentTimeMillis());
    routingContext.put(Constants.VERTICLE_TIMEOUT, 10000l);
    routingContext.put(Constants.ROUTING_BUS,vertx.eventBus());
    context.put(Constants.HTTP_CLIENT_VERTICLE_TIMEOUT, config().getLong(Constants.HTTP_CLIENT_VERTICLE_TIMEOUT, 10000l));

    routingContext.next();
  }

  private void sentWrongRequest(RoutingContext routingContext) {
    {
      HttpServerResponse resp = routingContext.request().response();
      resp.putHeader("Content-type", "application/json");
      resp.setStatusCode(400);//Bad request
      List<ResponseError> errs = new LinkedList<ResponseError>();
      errs.add(new ResponseError("RESOURCE_NOT_FOUND",
          "Requested Resource not found"));
      Response output = ResponseFactory.createFailureResp(routingContext, errs);
      resp.end(JsonUtils.object2Json(output).toString());
    }

  }
}